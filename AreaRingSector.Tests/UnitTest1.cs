using System;
using NUnit.Framework;

namespace AreaRingSector.Tests
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            double radius_big = 7;
            double radius_small = 5;
            double angle = 45;
            double expected_result = 9.42;
            double real_result = new AreaRingSector.Library.AreaRingSector().Calculation(radius_big, radius_small, angle);
            Assert.AreEqual(expected_result, real_result, 0.05);
        }

        [Test]
        public void Test2()
        {
            double radius_big = 7;
            double radius_small = -5;
            double angle = 45;
            Assert.Throws<ArgumentException>(() => new AreaRingSector.Library.AreaRingSector().Calculation(radius_big, radius_small, angle));
        }
    }
}