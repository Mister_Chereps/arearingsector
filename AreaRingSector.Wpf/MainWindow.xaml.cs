﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace AreaRingSector.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new AreaRingSectorViewModel();
        }

        private bool error = false; //Чтобы при первом запуске не вылезало предупреждение
        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text != "")
            {
                if(error == true)
                {
                    try
                    {
                        double check = double.Parse(textBox.Text);
                        if (double.Parse(textBox_radius_small.Text) > double.Parse(textBox_radius_big.Text))
                        {
                            string value = textBox_radius_big.Text;
                            textBox_radius_big.Text = textBox_radius_small.Text;
                            textBox_radius_small.Text = value;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Введите в поле число, большее нуля.\n\nКод ошибки: " + ex, "Ошибка.");
                    }
                }
                error = true;
            }
        }
    }
}
