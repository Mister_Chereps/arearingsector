﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AreaRingSector.Wpf
{
    class AreaRingSectorViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public AreaRingSectorViewModel()
        {
            radius_big = 3;
            radius_small = 1;
            angle = 90;
        }

        private double radius_big;
        private double radius_small;
        private double angle;

        public double Radius_big
        {
            get { return radius_big; }

            set
            {
                radius_big = value;
                OnPropertyChanged("Radius_big");
                OnPropertyChanged("Result");
            }
        }
        public double Radius_small
        {
            get { return radius_small; }

            set
            {
                radius_small = value;
                OnPropertyChanged("Radius_small");
                OnPropertyChanged("Result");
            }
        }

        public double Angle
        {
            get { return angle; }

            set
            {
                angle = value;
                OnPropertyChanged("Angle");
                OnPropertyChanged("Result");
            }
        }

        public double Result
        {
            get { return Math.Round(new AreaRingSector.Library.AreaRingSector().Calculation(radius_big, radius_small, angle), 2); }
        }
    }
}
