﻿using System;

namespace AreaRingSector.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Введите больший радиус - ");
                double radius_big = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите меньший радиус - ");
                double radius_small = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите угол - ");
                double angle = Convert.ToDouble(Console.ReadLine());
                if (radius_big < 0 || radius_small < 0)
                {
                    Console.WriteLine("Радиусы должны быть больше нуля.\n");
                    continue;
                }
                else if (angle < 0 || angle > 360)
                {
                    Console.WriteLine("Угол должен быть больше нуля и меньше 360.\n");
                    continue;
                }
                else
                {
                    double result = new AreaRingSector.Library.AreaRingSector().Calculation(radius_big, radius_small, angle);
                    System.Console.WriteLine("\nПлощадь сектора кольца - " + Math.Round(result, 2));
                    break;
                }
            }
        }
    }
}
