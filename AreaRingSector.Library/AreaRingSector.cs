﻿using System;

namespace AreaRingSector.Library
{
    public class AreaRingSector
    {
        public double Calculation(double radius_big, double radius_small, double angle)
        {
            if (radius_big < 0 || radius_small < 0)
                throw new ArgumentException("Один из радиусов меньше нуля.");
            if (angle < 0 || angle > 360)
                throw new ArgumentException("Угол меньше нуля или больше 360.");
            return Math.PI * angle * (radius_big * radius_big - radius_small * radius_small) / 360;
        }
    }
}
