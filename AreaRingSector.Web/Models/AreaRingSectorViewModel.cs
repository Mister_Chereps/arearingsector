﻿using System;

namespace AreaRingSector.Web.Models
{
    public class AreaRingSectorViewModel
    {
        public double radius_big { get; set; }
        public double radius_small { get; set; }
        public double angle { get; set; }
        public double result
        {
            get { return Math.Round(new AreaRingSector.Library.AreaRingSector().Calculation(radius_big, radius_small, angle), 2); }
        }
        public string report { get; set; }
    }
}
