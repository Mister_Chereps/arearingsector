﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using AreaRingSector.Web.Models;

namespace AreaRingSector.Web.Controllers
{
    public class AreaRingSectorController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var view_model = new AreaRingSectorViewModel();
            view_model.radius_big = 3;
            view_model.radius_small = 1;
            view_model.angle = 90;
            return View(view_model);
        }

        [HttpPost]
        public IActionResult Index(IFormCollection form)
        {
            var view_model = new AreaRingSectorViewModel();
            if (!form.TryGetValue("Radius_big", out var R_b) || !form.TryGetValue("Radius_small", out var R_s) || !form.TryGetValue("Angle", out var An))
            {
                R_b = "3";
                R_s = "1";
                An = "90";
                view_model.report = "Не были указаны данные.";
            }
            if (!Double.TryParse(R_b, out var Rb_value) || !Double.TryParse(R_s, out var Rs_value) || !Double.TryParse(An, out var An_value))
            {
                Rb_value = 3;
                Rs_value = 1;
                An_value = 90;
                view_model.report = "Указанное значение не является числовым.";
            }
            view_model.radius_big = Rb_value;
            view_model.radius_small = Rs_value;
            view_model.angle = An_value;
            return View(view_model);
        }
    }
}
